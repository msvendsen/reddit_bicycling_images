var info = {
	url: "data.php",
	rawData: null,
	lastTitle: null,
	$items: null
};

var display = {
	newData: function(data){
		for(var i = 0; i < data.length; i++){
			if(data[i].data.domain == "i.imgur.com"){
				var imageData = {
					imageUrl: data[i].data.url,
					author: data[i].data.author,
					title: data[i].data.title,
					permalink: data[i].data.permalink
				};
				display.addImage(imageData);
			}
			info.lastTitle = data[i].data.name;
		}
	},
	addImage: function(data){
		var $toAdd = $("<div class='span4 item'><div class='well'><a href='http://reddit.com" + data.permalink + "' target='_blank'><img src='" + data.imageUrl + "'></a><span class='title_text'>" + data.title + "</span><div><span class='badge badge-inverse'>" + data.author + "</span></div></div></div>");
		//$("#item_container").append(toAdd);
		$("#item_container").isotope("insert", $toAdd);
	},
	reLayout: function(){
		$("#item_container").isotope({
        	itemSelector : '.item'
      	});
	},
	more: function(){

	}
};

$(document).ready(function(){

	mql('all and (max-width: 480px)', display.reLayout);
  	mql('all and (max-width: 768px)', display.reLayout);
  	mql('all and (min-width: 980px)', display.reLayout);
  	mql('all and (min-width: 1200px)', display.reLayout);

	$.ajax({
		url: info.url,
		dataType: "json"
	}).done(function(data){
		info.rawData = data.data.children;
		display.newData(info.rawData);
	});

	$("#more_btn").click(function(){
		var urlToUse = info.url + "?after=" + info.lastTitle;
		$.ajax({
			url: urlToUse,
			dataType: "json"
		}).done(function(data){
			info.rawData = data.data.children;
			display.newData(info.rawData);
		});
	});

	$("#item_container").isotope({
    	itemSelector : '.item'
    });

});